package ua.goit.javaee;

import ua.goit.javaee.dao.jdbc.JdbcCustomerDaoImpl;
import ua.goit.javaee.dao.jdbc.JdbcDeveloperDaoImpl;
import ua.goit.javaee.dao.jdbc.JdbcProjectDaoImpl;
import ua.goit.javaee.model.Customer;
import ua.goit.javaee.model.Developer;
import ua.goit.javaee.model.Project;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Иван on 12.01.2017.
 */
public class MainForProjects {

    public static void main(String[] args) {

        JdbcProjectDaoImpl projectDao = JdbcProjectDaoImpl.getInstance();
        JdbcDeveloperDaoImpl developerDao = JdbcDeveloperDaoImpl.getInstance();
        JdbcCustomerDaoImpl customerDao = JdbcCustomerDaoImpl.getInstance();

        System.out.println("All projects is: ");
        projectDao.getAll().forEach(System.out::println);

        Set<Developer> developers = new HashSet<>();
        developers.add(developerDao.findById(4));
        Set<Customer> customers = new HashSet<>();
        customers.add(customerDao.findById(3));
        Project project = new Project(11, "Social Network", 1000000000, developers, customers);

        System.out.println("persisted project: " + projectDao.save(project));

        System.out.println("Project with id = 3 : " + projectDao.findById(3));

        projectDao.delete(project);

        System.out.println("\n\nAll projects is: ");
        projectDao.getAll().forEach(System.out::println);

    }
}
