package ua.goit.javaee;

import ua.goit.javaee.dao.jdbc.JdbcCompanyDaoImpl;
import ua.goit.javaee.dao.jdbc.JdbcDeveloperDaoImpl;
import ua.goit.javaee.dao.jdbc.JdbcSkillDaoImpl;
import ua.goit.javaee.model.Company;
import ua.goit.javaee.model.Developer;
import ua.goit.javaee.model.Skill;

import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public class Main {

  public static void main(String[] args) {

    JdbcDeveloperDaoImpl developerDao = JdbcDeveloperDaoImpl.getInstance();
    JdbcSkillDaoImpl skillDao = JdbcSkillDaoImpl.getInstance();

    System.out.println(skillDao.findById(4));
    System.out.println("=====");
    System.out.println(developerDao.findById(9));
    System.out.println("=====");

    Skill newSkill = skillDao.save(new Skill(9,"VBA"));

    Set<Skill> skillsFromDB = skillDao.getAll();
    System.out.println("All skills:");
    skillsFromDB.forEach(System.out::println);

    skillDao.delete(newSkill);

    skillsFromDB = skillDao.getAll();
    System.out.println("All skills after update table: ");
    skillsFromDB.forEach(System.out::println);


    Set<Skill> skills = new HashSet<>();
    skills.add(new Skill(1, "java"));
    skills.add(new Skill(3, "c#"));
//    skills.add(new Skill(5, "python"));
//    Developer developer = new Developer(17, "Roman", "Kopach", 32, 1100, skills);
    for (int i = 15; i < 250;  i++)
      developerDao.save(new Developer(i, "Roman1 ", "Kopach", 32, 1100, skills));
//      developerDao.delete(new Developer(i, "Roman", "Kopach", 32, 1100, skills));
    Set<Developer> developers = developerDao.getAll();
    developers.forEach(System.out::println);
    System.out.println("=====");

    JdbcCompanyDaoImpl companyDao = JdbcCompanyDaoImpl.getInstance();
    System.out.println(companyDao.findById(2));

    Set<Company> companies = companyDao.getAll();
    companies.forEach(System.out::println);
    System.out.println("=====");

  }

}
