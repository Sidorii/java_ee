﻿-- 1. Добавить разаработчикам поле (salary - зарплата).

ALTER TABLE developers
  ADD COLUMN salary INT;

UPDATE developers
SET salary = 100500
WHERE id = 1;

UPDATE developers
SET salary = 10000
WHERE id = 2;

UPDATE developers
SET salary = 94324
WHERE id = 3;

UPDATE developers
SET salary = 9999999
WHERE id = 4;

UPDATE developers
SET salary = 10
WHERE id = 5;

UPDATE developers
SET salary = 189
WHERE id = 6;

UPDATE developers
SET salary = 15
WHERE id = 7;

UPDATE developers
SET salary = 7
WHERE id = 8;

UPDATE developers
SET salary = 5956312
WHERE id = 9;

UPDATE developers
SET salary = 777
WHERE id = 10;

SELECT *
FROM developers;
