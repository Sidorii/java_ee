﻿/*
Домашнее задание #1.1

Необходимо создать БД, которая содержит следующие таблицы:

- developers (хранит данные о разработчиках)
- skills (навыки разработчиков – Java, C++, etc.)
- projects (проекты, на которых работают разработчики)
- companies (IT компании, в которых работают разработчики)
- customers (клиенты, которые являются заказчиками проектов в IT компаниях)

При этом:

- разработчики могут иметь 2 недели много навыков
- каждый проект имеет много разработчиков, которые над ним работают
- компании выполняют много проектов одновременно - заказчики имеют много проектов

Необходимо реализовать как таблицы, так и грамотные связи между ними.
Результатом выполнения задания являеются файлы initDB.sql (создание таблиц и связей между ними), populateDB.sql (заполнение таблиц данными)
*/

DROP TABLE IF EXISTS
developers,
skills,
companies,
customers,
projects,
developer_skills,
project_developers,
project_customers,
company_projects;

CREATE TABLE IF NOT EXISTS developers (
  id         SERIAL NOT NULL,
  first_name TEXT   NOT NULL,
  last_name  TEXT,
  age        INT,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS skills (
  id   SERIAL NOT NULL,
  name TEXT   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS companies (
  id   SERIAL NOT NULL,
  name TEXT   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS customers (
  id   SERIAL NOT NULL,
  name TEXT   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS projects (
  id   SERIAL NOT NULL,
  name TEXT   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS developer_skills (
  developer_id INT NOT NULL,
  skill_id     INT NOT NULL,

  FOREIGN KEY (developer_id) REFERENCES developers (id),
  FOREIGN KEY (skill_id) REFERENCES skills (id),

  UNIQUE (developer_id, skill_id)
);

CREATE TABLE IF NOT EXISTS project_developers (
  project_id   INT NOT NULL,
  developer_id INT NOT NULL,

  FOREIGN KEY (project_id) REFERENCES projects (id),
  FOREIGN KEY (developer_id) REFERENCES developers (id),

  UNIQUE (project_id, developer_id)
);

CREATE TABLE IF NOT EXISTS project_customers (
  project_id  INT NOT NULL,
  customer_id INT NOT NULL,

  FOREIGN KEY (project_id) REFERENCES projects (id),
  FOREIGN KEY (customer_id) REFERENCES customers (id),

  UNIQUE (project_id, customer_id)
);

CREATE TABLE IF NOT EXISTS company_projects (
  company_id  INT NOT NULL,
  project_id  INT NOT NULL,

  FOREIGN KEY (company_id) REFERENCES companies (id),
  FOREIGN KEY (project_id) REFERENCES projects (id),

  UNIQUE (company_id, project_id)
);
