-- 4. Добавить поле (cost - стоимость) в таблицу Projects.

ALTER TABLE projects
  ADD COLUMN cost INT;

UPDATE projects
SET cost = 500000
WHERE id = 1;

UPDATE projects
SET cost = 150000
WHERE id = 2;

UPDATE projects
SET cost = 1000000
WHERE id = 3;

UPDATE projects
SET cost = 159000
WHERE id = 4;

UPDATE projects
SET cost = 650000
WHERE id = 5;

UPDATE projects
SET cost = 2000000
WHERE id = 6;

SELECT *
FROM projects;
