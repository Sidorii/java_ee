﻿-- populateDB.sql (заполнение таблиц данными)

INSERT INTO developers (first_name, last_name, age)
VALUES
  ('Taras', 'Lavrenyuk', 19),
  ('Vasiliy', 'Kylik', 25),
  ('Ivan', 'Grozniy', 150),
  ('Pablo', 'Eskobaro', 63),
  ('Super', 'Developer', 27),
  ('Ernest', 'Heminguei', 78),
  ('Artur', 'Liliput', 10),
  ('Andriy', 'Shevchenko', 41),
  ('Donald', 'Tramp', 45),
  ('Dzheki', 'Chan', 25);

INSERT INTO skills (name)
VALUES
  ('java'),
  ('c++'),
  ('c#'),
  ('ruby'),
  ('python'),
  ('golang'),
  ('html/css'),
  ('javascript');

INSERT INTO companies (name)
VALUES
  ('Wonderful Software'),
  ('EuroProga'),
  ('BossCode');

INSERT INTO customers (name)
VALUES
  ('Google'),
  ('Facebook'),
  ('Twitter');

INSERT INTO projects (name)
VALUES
  ('Super search'),
  ('Great look'),
  ('New design'),
  ('Animation feature'),
  ('Music add-on'),
  ('Video message');

INSERT INTO developer_skills (developer_id, skill_id)
VALUES
  (1, 1),
  (1, 3),
  (1, 5),
  (2, 7),
  (2, 8),
  (3, 2),
  (3, 4),
  (3, 5),
  (4, 1),
  (4, 4),
  (5, 1),
  (5, 7),
  (5, 3),
  (6, 2),
  (6, 4),
  (7, 3),
  (7, 5),
  (8, 1),
  (8, 2),
  (9, 6),
  (9, 7),
  (10, 6),
  (10, 1);

INSERT INTO project_developers (project_id, developer_id)
VALUES
  (1, 1),
  (1, 3),
  (1, 5),
  (1, 7),
  (1, 8),
  (2, 2),
  (2, 10),
  (2, 8),
  (2, 9),
  (2, 4),
  (3, 3),
  (3, 7),
  (3, 6),
  (3, 4),
  (4, 2),
  (4, 6),
  (4, 8),
  (4, 1),
  (4, 10),
  (5, 9),
  (5, 5),
  (5, 2),
  (5, 7),
  (5, 3),
  (6, 8),
  (6, 1),
  (6, 7),
  (6, 3),
  (6, 4);

INSERT INTO project_customers (project_id, customer_id)
VALUES
  (1, 1),
  (2, 2),
  (3, 3),
  (4, 1),
  (5, 2),
  (6, 3);

INSERT INTO company_projects (company_id, project_id)
VALUES
  (1, 1),
  (2, 2),
  (3, 3),
  (1, 4),
  (2, 5),
  (3, 6);
