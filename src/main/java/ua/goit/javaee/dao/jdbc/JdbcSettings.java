package ua.goit.javaee.dao.jdbc;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
class JdbcSettings {

    static final String JDBC_DRIVER_CLASS;
    static final String JDBC_URL;
    static final String JDBC_USER;
    static final String JDBC_PASSWORD;

    static {

        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("classes/jdbc.properties"));
            JDBC_DRIVER_CLASS = properties.getProperty("jdbc.driver.class");
            JDBC_URL = properties.getProperty("jdbc.url");
            JDBC_USER = properties.getProperty("jdbc.user");
            JDBC_PASSWORD = properties.getProperty("jdbc.password");

        } catch (IOException ioe) {
            throw new IllegalStateException(ioe);
        }
    }
}
