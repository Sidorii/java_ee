package ua.goit.javaee.dao.jdbc;

import org.slf4j.LoggerFactory;
import ua.goit.javaee.dao.SkillDao;
import ua.goit.javaee.model.Skill;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by Иван on 15.12.2016.
 */
public class JdbcSkillDaoImpl extends JdbcDataSource implements SkillDao {

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private JdbcSkillDaoImpl(){

    }

    public static JdbcSkillDaoImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static final class SingletonHolder {
        private static final JdbcSkillDaoImpl INSTANCE = new JdbcSkillDaoImpl();
    }

    @Override
    public Skill findById(int id) {

        String query =
                "SELECT * " +
                        "FROM skills " +
                        "WHERE id = ?";

        try (Connection connection = getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {

                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {

                    if (resultSet.next()) {
                        LOGGER.info("Entry found by id successfully");
                        return getSkill(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error("SQL Exception in method \"findById()\", " + e);
        }
        return null;
    }

    private Skill getSkill(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        return new Skill(id, name);
    }


    @Override
    public Set<Skill> getAll() {

        String query =
                "SELECT * " +
                        "FROM skills";

        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {

                Set<Skill> resultEntries;
                try (ResultSet resultSet = statement.executeQuery(query)) {
                    resultEntries = new HashSet<>();

                    while (resultSet.next()) {
                        resultEntries.add(getSkill(resultSet));
                    }
                }
                LOGGER.info("All skills got from DB successfully");
                return resultEntries;
            }
        } catch (SQLException e) {
            LOGGER.error("SQL Exception in method \"getAll()\", " + e);
        }
        return null;
    }

    @Override
    public Skill save(Skill skill) {
        if (skill == null) {
            return null;
        }

        //если скилл уже внесен в БД, он не сохраняеться.
        //и Возвращается екземпляр с БД.
        Skill skillFromDB = findById(skill.getId());
        if (skillFromDB != null) {
            LOGGER.error("Skill with id=" + skillFromDB.getId() + " already exist in DB");
            return skillFromDB;
        }

        String query =
                "INSERT INTO skills " +
                        "VALUES(?,?);";

        try (Connection connection = getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {

                preparedStatement.setInt(1, skill.getId());
                preparedStatement.setString(2, skill.getName());

                int addRows = preparedStatement.executeUpdate();
                LOGGER.info("Successfully added " + addRows + " rows");
                return skill;
            }
        } catch (SQLException e) {
            LOGGER.error("Could not add skill in DB," + e);
        }
        return null;
    }

    @Override
    public boolean delete(Skill skill) {
        if (skill == null) {
            return true;
        }

        String query = "DELETE FROM skills " +
                "WHERE id = " + skill.getId();

        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {

                int rows = statement.executeUpdate(query);

                if (rows != 0) {
                    LOGGER.info("Successfully deleted " + rows + " rows");
                    return true;
                } else {
                    LOGGER.info("No such skill in skills table");
                    return false;
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
        }
        return false;
    }
}
