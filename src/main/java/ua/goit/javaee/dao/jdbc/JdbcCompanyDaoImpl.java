package ua.goit.javaee.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.goit.javaee.dao.CompanyDao;
import ua.goit.javaee.model.Company;
import ua.goit.javaee.model.Customer;
import ua.goit.javaee.model.Developer;
import ua.goit.javaee.model.Project;

import java.sql.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Vasiliy Kylik on 14.12.2016.
 */
public class JdbcCompanyDaoImpl extends JdbcDataSource implements CompanyDao {


    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcCompanyDaoImpl.class);


    // Правильный Singleton в Java  3 On Demand Holder idiom

    /* + Ленивая инициализация
     + Высокая производительность
     - Невозможно использовать для не статических полей класса*/


    public JdbcCompanyDaoImpl() {
    }

    private static final class SingletonHolder {
        private static final JdbcCompanyDaoImpl INSTANCE = new JdbcCompanyDaoImpl();
    }

    public static JdbcCompanyDaoImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public Company findById(int id) {
        String sql = "" +
                "SELECT * " +
                "FROM companies " +
                "WHERE id = ?";

        try (Connection connection = getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, id);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        return getCompany(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
        }

        return null;
    }

    @Override
    public Set<Company> getAll() {
        Set<Company> companies = new HashSet<>();
        String sql = "SELECT * FROM companies";
        try (Connection connection = getConnection(); Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) {
                companies.add(getCompany(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
        }
        return companies;
    }

    @Override
    public Company save(Company company) {
        if (company == null) {
            return null;
        }
        // проверка есть ли в БД компания которую мы хотим сохранить
        int id = company.getId();
        Company companyInDb = findById(id);
        // если его нет в БД то добавляем
        if (companyInDb == null) {
            String sql = "INSERT INTO Companies (id,name) VALUES (?,?)";
            try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, id);
                statement.setString(2, company.getName());
                statement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error("Exception occurred while connection to DB, e");
                return null;
            }
            // сохраним проекты компании
            saveCompanyProjects(id, company.getProjects());
            return company;
        }
        // если сохраняемая компания уже есть в БД,
        // но у нее изменились какието поля,
        // то отправляем ее на обновление
        if (!company.equals(companyInDb)) {
            return update(company);
        }
        return company;
    }

    private Company update(Company company) {
        String sql = "UPDATE companies SET name = ? WHERE id = ?";
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(2, company.getId());
            statement.setString(1, company.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
            return null;
        }
        // сохраняем проекты компании
        saveCompanyProjects(company.getId(), company.getProjects());
        return company;
    }


    @Override
    public boolean delete(Company company) {
        int id = company.getId();
        // перед удалением компании удаляем его проекты из соединительной таблицы
        deleteCompanyProjects(id);
        String sql = "DELETE FROM companies WHERE id = ?";
        return executeUpdateById(id, sql);
    }

    private Company getCompany(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        Set<Project> projects = findProjectsByCompanyId(id);
        return new Company(id, name, projects);
    }

    private Set<Project> findProjectsByCompanyId(int companyId) {
        String sql =
                // данный запрос вернет проекты (id,name,cost) компании с указанным id
                "SELECT " +
                        "projects.id AS id, " +
                        "projects.name AS name, " +
                        "projects.cost AS cost " +
                        "FROM company_projects " +
                        "INNER JOIN companies  ON company_projects.company_id = companies.id " +
                        "INNER JOIN projects ON company_projects.project_id = projects.id " +
                        "WHERE companies.id = ?";

        Set<Project> projects = new HashSet<>();

        try (Connection connection = getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, companyId);
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        int projectId = resultSet.getInt("id");
                        String name = resultSet.getString("name");
                        int cost = resultSet.getInt("cost");
                        // мы создадим объект типа Project в который передадим sets
                        Set<Developer> developers = JdbcDeveloperDaoImpl.getInstance().findByProjectId(projectId);
                        Set<Customer> customers = JdbcCustomerDaoImpl.getInstance().findByProjectId(projectId);
                        projects.add(new Project(projectId, name, cost, developers, customers));
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
        }

        return projects;
    }

    private void saveCompanyProjects(Integer companyid, Set<Project> projects) {
        Company inputCompany = findById(companyid);
        if (projects == null) {
            LOGGER.error("This company " + inputCompany + " has no projects");
        }
        // перед добавлением проектов компании
        // удаляем все те что были в БД
        deleteCompanyProjects(companyid);
        if (projects.isEmpty()) {
            LOGGER.info("Промежуточное удаление проектов компании закончено, можно записывать");
        }
        // для того что бы записать за одну операцию
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < projects.size(); i++) {
            if (stringBuilder.length() != 0) {
                stringBuilder.append(", (?, ?)");
            } else {
                stringBuilder.append("(?, ?)");
            }
        }

        String sql = "Insert into company_projects (company_id, project_id) Values " + stringBuilder.toString();

        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
            Iterator<Project> iterator = projects.iterator();
            // позиции
            int companyIdParameterIndex = 1;
            int projectParameterIndex = 2;
            while (iterator.hasNext()) {
                Project project = iterator.next();
                statement.setInt(companyIdParameterIndex, companyid);
                statement.setInt(projectParameterIndex, project.getId());
                // смещение по позициям - для двухсвязной таблицы равно двум
                companyIdParameterIndex += 2;
                projectParameterIndex += 2;
            }
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
        }
    }

    private boolean deleteCompanyProjects(int id) {
        String sql = "Delete from company_projects where company_id = ?";
        return executeUpdateById(id, sql);
    }

    // принимает запрос SQL и применяет к объекту с определенным id
    private boolean executeUpdateById(int id, String sql) {
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB", e);
            return false;
        }
        return true;
    }
}
