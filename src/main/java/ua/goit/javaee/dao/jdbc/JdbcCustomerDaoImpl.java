package ua.goit.javaee.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.goit.javaee.dao.CustomerDao;
import ua.goit.javaee.model.Customer;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by Raketa on 30.12.2016.
 */


public class JdbcCustomerDaoImpl extends JdbcDataSource implements CustomerDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcCustomerDaoImpl.class);

    private JdbcCustomerDaoImpl() {
        super();
    }

    public static JdbcCustomerDaoImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }


    private static final class SingletonHolder {
        private static final JdbcCustomerDaoImpl INSTANCE = new JdbcCustomerDaoImpl();
    }

    @Override
    public Customer findById(int id) {
        Customer customer;

        try (Connection connection = getConnection()) {
            String sql = "SELECT * FROM customers WHERE id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                customer = getCustomer(resultSet);
                resultSet.close();
                return customer;
            }
        } catch (SQLException exc) {
            LOGGER.error(exc.toString());
            return null;
        }
    }

    @Override
    public Set<Customer> getAll() {
        Set<Customer> customers = new HashSet<>();

        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                String sql = "SELECT * FROM customers";
                ResultSet resultSet = statement.executeQuery(sql);

                while (resultSet.next()) {
                    customers.add(getCustomer(resultSet));
                }
                LOGGER.info(customers.size() + " entries selected from DB.");
                resultSet.close();
            }
        } catch (SQLException exc) {
            LOGGER.error(exc.toString());
        } finally {
            return customers;
        }
    }

    @Override
    public Customer save(Customer customer) {

        try (Connection connection = getConnection()) {
            String sqlSave = "INSERT INTO customers " +
                    "VALUES(?,?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlSave)) {
                preparedStatement.setInt(1, customer.getId());
                preparedStatement.setString(2, customer.getName());
                int updatedRows = preparedStatement.executeUpdate();
                LOGGER.info(updatedRows + " row inserted in DB.");
                return customer;
            }
        } catch (SQLException exc) {
            LOGGER.error(exc.toString());
            return null;
        }
    }

    @Override
    public boolean delete(Customer customer) {
        try (Connection connection = getConnection()) {
            String sqlDelDependency = "DELETE FROM project_customers WHERE customer_id = ?";
            String sqlDelEntry = "DELETE FROM customers WHERE id = ?";

            int deletedDependencies;
            int deleteEntries;

            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlDelDependency)) {
                preparedStatement.setInt(1, customer.getId());
                deletedDependencies = preparedStatement.executeUpdate();
            }

            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlDelEntry)) {
                preparedStatement.setInt(1, customer.getId());
                deleteEntries = preparedStatement.executeUpdate();
            }
            LOGGER.info("After DELETE operation was deleted " + deletedDependencies +
                    " dependencies and " + deleteEntries + " entries.");

            return deleteEntries > 0;
        } catch (SQLException exc) {
            LOGGER.error(exc.toString());
            return false;
        }
    }


    // method that returns set of Customers for JdbcCustomerDaoImpl
    public Set<Customer> findByProjectId(int projectId) {
        Set<Customer> developers = new HashSet<>();
        String sql = "SELECT customers.id AS id, customers.name AS name FROM project_customers\n" +
                "\n" +
                "INNER JOIN projects ON project_customers.project_id = projects.id\n" +
                "INNER JOIN customers ON project_customers.customer_id = customers.id\n" +
                "\n" +
                "WHERE projects.id = ?";

        try (Connection connection = getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, projectId);
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        developers.add(getCustomer(resultSet));
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Exception occurred while connection to DB (findByProjectId in JdbcCustomerDaoImpl)", e);
        }
        return developers;
    }

    private Customer getCustomer(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        return new Customer(id, name);
    }
}
