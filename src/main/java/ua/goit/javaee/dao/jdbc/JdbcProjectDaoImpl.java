package ua.goit.javaee.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.goit.javaee.dao.ProjectDao;
import ua.goit.javaee.model.Customer;
import ua.goit.javaee.model.Developer;
import ua.goit.javaee.model.Project;

import java.sql.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Иван on 12.01.2017.
 */
public class JdbcProjectDaoImpl extends JdbcDataSource implements ProjectDao {


    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private JdbcProjectDaoImpl() {
        super();
    }

    public static JdbcProjectDaoImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final JdbcProjectDaoImpl INSTANCE = new JdbcProjectDaoImpl();
    }


    @Override
    public Project findById(int id) {

        try (Connection connection = getConnection()) {
            String sql = "SELECT * FROM projects WHERE id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    LOGGER.info("Priject with id = " + id + " successfully found from DB.");
                    return getProject(resultSet);
                }
                LOGGER.info("Project with id = " + id + " does not exist in DB.");
                return null;
            }
        } catch (SQLException exc) {
            LOGGER.error(exc.toString());
            return null;
        }
    }

    @Override
    public Set<Project> getAll() {
        Set<Project> projects = new HashSet<>();

        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {

                String sql = "SELECT * FROM projects";
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    projects.add(getProject(resultSet));
                }
                LOGGER.info(projects.size() + " - is all projects in DB.");
                return projects;
            }
        } catch (SQLException exc) {
            LOGGER.error(exc.toString());
            throw new RuntimeException(exc);
        }
    }

    @Override
    public Project save(Project project) {
        Connection connection = null;
        Savepoint savepoint = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);

            String sql = "INSERT INTO projects " +
                    "VALUES(?,?,?)";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                savepoint = connection.setSavepoint();

                preparedStatement.setInt(1, project.getId());
                preparedStatement.setString(2, project.getName());
                preparedStatement.setInt(3, project.getCost());
                int instRows = preparedStatement.executeUpdate();


                connection.commit();

                saveDependenciesInProject_Customers(connection, project);
                saveDependenciesInProject_Developers(connection, project);
                connection.commit();

                if (instRows > 0) {
                    LOGGER.info(project + " successfully inserted into DB.");
                    return project;
                } else {
                    LOGGER.info("no project inserted into DB.");
                    return null;
                }
            }

        } catch (SQLException exc) {
            if (connection != null && savepoint != null) {
                try {
                    connection.rollback(savepoint);
                } catch (SQLException e) {
                    LOGGER.error(e.toString());
                }
            }
            LOGGER.error(exc.toString());
            throw new RuntimeException(exc);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.toString());
                }
            }
        }
    }

    private int saveDependenciesInProject_Developers(Connection connection, Project project) throws SQLException {

        String sqlPD = "INSERT INTO project_developers VALUES ";
        for (int i = 0; i < project.getDevelopers().size(); i++) {
            sqlPD += "(" + project.getId() + ",?),";
        }

        sqlPD = sqlPD.substring(0, sqlPD.length() - 1);

        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlPD)) {
            Iterator<Developer> iterator = project.getDevelopers().iterator();
            int i = 0;
            while (iterator.hasNext()) {
                preparedStatement.setInt(++i, iterator.next().getId());
            }
            int insertedRows = preparedStatement.executeUpdate();
            LOGGER.info("In project_developers was inserted " + insertedRows + " rows.");
            connection.commit();
            return insertedRows;
        }

    }

    private int saveDependenciesInProject_Customers(Connection connection, Project project) throws SQLException {
        //generic query
        String sqlPC = "INSERT INTO project_customers VALUES ";
        for (int i = 0; i < project.getCustomers().size(); i++) {
            sqlPC += "(" + project.getId() + ",?),";
        }

        sqlPC = sqlPC.substring(0, sqlPC.length() - 1);
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlPC)) {
            Iterator<Customer> iterator = project.getCustomers().iterator();
            int i = 0;
            while (iterator.hasNext()) {
                preparedStatement.setInt(++i, iterator.next().getId());
            }
            int insertedRows = preparedStatement.executeUpdate();
            LOGGER.info("In project_customers was inserted " + insertedRows + " rows.");
            return insertedRows;
        }

    }

    @Override
    public boolean delete(Project project) {
        Connection connection = null;

        try {
            connection = getConnection();
            connection.setAutoCommit(false);

            String sqlPCDel = "DELETE FROM project_customers WHERE project_id = ?";
            String sqlPDDel = "DELETE FROM project_developers WHERE project_id = ?";
            String sqlDel = "DELETE FROM projects WHERE id = ?";
            int dltRows = 0;

            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlPCDel)) {
                preparedStatement.setInt(1, project.getId());
                dltRows += preparedStatement.executeUpdate();
            }

            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlPDDel)) {
                preparedStatement.setInt(1, project.getId());
                dltRows += preparedStatement.executeUpdate();
            }

            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlDel)) {
                preparedStatement.setInt(1, project.getId());
                dltRows += preparedStatement.executeUpdate();
            }
            connection.commit();
            if (dltRows > 0) {
                LOGGER.info(project + " successfully deleted from DB. Deleted rows: " +
                        dltRows);
                return true;
            } else {
                LOGGER.info("No project deleted from DB.Deleted rows: " + dltRows);
                return false;
            }
        } catch (SQLException exc) {
            if(connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e) {
                    LOGGER.error(e.toString());
                    throw new RuntimeException(e);
                }
            }
            LOGGER.error(exc.toString());
            throw new RuntimeException(exc);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.toString());
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private Project getProject(ResultSet resultSet) throws SQLException {

        int id = resultSet.getInt(1);
        String name = resultSet.getString(2);
        int cost = resultSet.getInt(3);

        Set<Customer> customers = findCustomersByProjectId(id);
        Set<Developer> developers = findDevelopersByProjectId(id);

        return new Project(id, name, cost, developers, customers);
    }

    private Set<Customer> findCustomersByProjectId(int id) {

        Set<Customer> customers = new HashSet<>();

        try (Connection connection = getConnection()) {
            String sql = "SELECT id, name " +
                    "FROM customers" +
                    "  INNER JOIN project_customers" +
                    "    ON customers.id = project_customers.customer_id" +
                    " WHERE project_customers.project_id = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();

                int customer_id;
                String name;

                while (resultSet.next()) {
                    customer_id = resultSet.getInt(1);
                    name = resultSet.getString(2);
                    customers.add(new Customer(customer_id, name));
                }
                LOGGER.info(customers.size() + " customers in project with id = " + id);
                return customers;
            }
        } catch (SQLException exc) {
            LOGGER.error(exc.toString());
            return customers;
        }
    }

    private Set<Developer> findDevelopersByProjectId(int id) {

        Set<Developer> developers = new HashSet<>();

        try (Connection connection = getConnection()) {
            String sql = "SELECT " +
                    "  id,first_name,last_name,age,salary " +
                    "FROM developers " +
                    "  INNER JOIN project_developers " +
                    "    ON developers.id = project_developers.developer_id " +
                    "WHERE project_id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();

                int developer_id;
                String firstName;
                String lastName;
                short age;
                int salary;

                while (resultSet.next()) {
                    developer_id = resultSet.getInt(1);
                    firstName = resultSet.getString(2);
                    lastName = resultSet.getString(3);
                    age = resultSet.getShort(4);
                    salary = resultSet.getInt(5);
                    developers.add(new Developer(developer_id, firstName, lastName, age, salary));
                }
                LOGGER.info(developers.size() + " developers works in project with id = " + id);
                return developers;
            }
        } catch (SQLException exc) {
            LOGGER.error(exc.toString());
            return developers;
        }
    }
}
