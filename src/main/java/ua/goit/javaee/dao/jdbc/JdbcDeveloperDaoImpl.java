package ua.goit.javaee.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.goit.javaee.dao.DeveloperDao;
import ua.goit.javaee.model.Developer;
import ua.goit.javaee.model.Skill;

import java.sql.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public class JdbcDeveloperDaoImpl extends JdbcDataSource implements DeveloperDao {

  private static final Logger LOGGER = LoggerFactory.getLogger(JdbcDeveloperDaoImpl.class);

  private JdbcDeveloperDaoImpl() {
  }

  public static JdbcDeveloperDaoImpl getInstance() {
    return SingletonHolder.INSTANCE;
  }

  private static final class SingletonHolder {
    private static final JdbcDeveloperDaoImpl INSTANCE = new JdbcDeveloperDaoImpl();
  }

  @Override
  public Developer findById(int id) {
    String sql = "" +
        "SELECT * " +
        "FROM developers " +
        "WHERE id = ?";

    try (Connection connection = getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(sql)) {
        statement.setInt(1, id);
        try (ResultSet resultSet = statement.executeQuery()) {
          if (resultSet.next()) {
            return getDeveloper(resultSet);
          }
        }
      }
    } catch (SQLException e) {
      LOGGER.error("Exception occurred while connection to DB", e);
    }

    return null;
  }

  @Override
  public Set<Developer> getAll() {
    Set<Developer> developers = new HashSet<>();
    String sql = "" +
        "SELECT * " +
        "FROM developers";

    try (Connection connection = getConnection()) {
      try (Statement statement = connection.createStatement()) {
        try (ResultSet resultSet = statement.executeQuery(sql)) {
          while (resultSet.next()) {
            developers.add(getDeveloper(resultSet));
          }
        }
      }
    } catch (SQLException e) {
      LOGGER.error("Exception occurred while connection to DB", e);
    }

    return developers;
  }

  @Override
  public Developer save(Developer developer) {
    if (developer == null) {
      return null;
    }

    // делаем проверку есть ли в ДБ девелопер которого мы хотим сохранить
    int id = developer.getId();
    Developer developerInDb = findById(id);

    // если его нет в ДБ то добавляем
    if (developerInDb == null) {
      String sql = "" +
          "INSERT INTO developers (id, first_name, last_name, age, salary) " +
          "VALUES (?, ?, ?, ?, ?)";
      try (Connection connection = getConnection()) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
          statement.setInt(1, id);
          statement.setString(2, developer.getFirstName());
          statement.setString(3, developer.getLastName());
          statement.setInt(4, developer.getAge());
          statement.setInt(5, developer.getSalary());
          statement.executeUpdate();
        }
      } catch (SQLException e) {
        LOGGER.error("Exception occurred while connection to DB", e);
        return null;
      }
      // сохраняем скилы девелопера
      saveDeveloperSkills(id, developer.getSkills());
      return developer;
    }

    // если сохраняемый девелопер уже есть в ДБ,
    // однако у него изменились какие-то поля,
    // то отправляем его на обновление
    if (!developer.equals(developerInDb)) {
      return update(developer);
    }

    return developer;
  }

  private Developer update(Developer developer) {
    String sql = "" +
        "UPDATE developers " +
        "SET first_name = ?, last_name = ?, age = ?, salary = ? " +
        "WHERE id = ?";

    try (Connection connection = getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(sql)) {
        statement.setInt(5, developer.getId());
        statement.setString(1, developer.getFirstName());
        statement.setString(2, developer.getLastName());
        statement.setInt(3, developer.getAge());
        statement.setInt(4, developer.getSalary());
        statement.executeUpdate();
      }
    } catch (SQLException e) {
      LOGGER.error("Exception occurred while connection to DB", e);
      return null;
    }

    // сохраняем скилы девелопера
    saveDeveloperSkills(developer.getId(), developer.getSkills());
    return developer;
  }

  @Override
  public boolean delete(Developer developer) {
    int id = developer.getId();
    // перед удалением девелопера удаляем его скилы из крос таблицы (developer_skills)
    deleteDeveloperSkills(id);
    String sql = "DELETE FROM developers WHERE id = ?";
    return executeUpdateById(id, sql);
  }

  private boolean deleteDeveloperSkills(int id) {
    String sql = "DELETE FROM developer_skills WHERE developer_id = ?";
    return executeUpdateById(id, sql);
  }

  private boolean executeUpdateById(int id, String sql) {
    try (Connection connection = getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(sql)) {
        statement.setInt(1, id);
        statement.executeUpdate();
      }
    } catch (SQLException e) {
      LOGGER.error("Exception occurred while connection to DB", e);
      return false;
    }
    return true;
  }

  private Developer getDeveloper(ResultSet resultSet) throws SQLException {
    int id = resultSet.getInt("id");
    String firstName = resultSet.getString("first_name");
    String lastName = resultSet.getString("last_name");
    int age = resultSet.getInt("age");
    int salary = resultSet.getInt("salary");
    Set<Skill> skills = findSkillsByDeveloperId(id);
    return new Developer(id, firstName, lastName, age, salary, skills);
  }

  private Set<Skill> findSkillsByDeveloperId(int developerId) {
    String sql =
        "SELECT " +
            "skills.id AS id, " +
            "skills.name AS name " +
            "FROM developer_skills " +
            "JOIN skills ON developer_skills.skill_id = skills.id " +
            "WHERE developer_id = ?";

    Set<Skill> skills = new HashSet<>();

    try (Connection connection = getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(sql)) {
        statement.setInt(1, developerId);
        try (ResultSet resultSet = statement.executeQuery()) {
          while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            skills.add(new Skill(id, name));
          }
        }
      }
    } catch (SQLException e) {
      LOGGER.error("Exception occurred while connection to DB", e);
    }

    return skills;
  }

  private void saveDeveloperSkills(int developerId, Set<Skill> skills) {
    if (skills == null) {
      return;
    }

    // перед добавлением скилов девелоперу
    // удаляем все те что уже были в ДБ
    deleteDeveloperSkills(developerId);

    if (skills.isEmpty()) {
      return;
    }

    // для того чтобы добавить за одну операцию
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < skills.size(); i++) {
      if (stringBuilder.length() != 0) {
        stringBuilder.append(", (?, ?)");
      } else {
        stringBuilder.append("(?, ?)");
      }
    }
    String sql = "" +
        "INSERT INTO developer_skills (developer_id, skill_id) " +
        "VALUES " + stringBuilder.toString();

    try (Connection connection = getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(sql)) {
        Iterator<Skill> iterator = skills.iterator();
        int developerIdParameterIndex = 1;
        int skillIdParameterIndex = 2;
        while (iterator.hasNext()) {
          Skill skill = iterator.next();
          statement.setInt(developerIdParameterIndex, developerId);
          statement.setInt(skillIdParameterIndex, skill.getId());
          developerIdParameterIndex += 2;
          skillIdParameterIndex += 2;
        }
        statement.executeUpdate();
      }
    } catch (SQLException e) {
      LOGGER.error("Exception occurred while connection to DB", e);
    }

  }

  // method that returns set of Developers for JdbcCompanyDaoImpl
  public Set<Developer> findByProjectId(int projectId) {
    Set<Developer> developers = new HashSet<>();
    String sql = "SELECT " +
            "developers.id AS id, " +
            "developers.first_name AS first_name, " +
            "developers.last_name AS last_name, " +
            "developers.age AS age, " +
            "developers.salary AS salary " +
            "FROM project_developers " +
            "INNER JOIN projects ON project_developers.project_id = projects.id " +
            "INNER JOIN developers ON project_developers.developer_id = developers.id " +
            "WHERE projects.id = ?";

    try (Connection connection = getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(sql)) {
        statement.setInt(1, projectId);
        try (ResultSet resultSet = statement.executeQuery()) {
          while (resultSet.next()) {
            developers.add(getDeveloper(resultSet));
          }
        }
      }
    } catch (SQLException e) {
      LOGGER.error("Exception occurred while connection to DB (findByProjectId in JdbcDeveloperDaoImpl)", e);
    }
    return developers;
  }
}