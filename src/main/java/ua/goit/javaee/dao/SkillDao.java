package ua.goit.javaee.dao;

import ua.goit.javaee.model.Skill;

/**
 * Created by Иван on 15.12.2016.
 */
public interface SkillDao extends AbstractDao<Skill> {

}
