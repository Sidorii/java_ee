package ua.goit.javaee.dao;

import ua.goit.javaee.model.Company;
import ua.goit.javaee.model.Developer;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public interface CompanyDao extends AbstractDao<Company> {

}
