package ua.goit.javaee.dao;

import ua.goit.javaee.model.Customer;

/**
 * Created by Raketa on 30.12.2016.
 */
public interface CustomerDao extends AbstractDao<Customer> {
}
