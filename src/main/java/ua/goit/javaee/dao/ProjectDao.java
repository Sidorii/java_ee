package ua.goit.javaee.dao;

import ua.goit.javaee.model.Project;

/**
 * Created by Иван on 12.01.2017.
 */
public interface ProjectDao extends AbstractDao<Project> {
}
