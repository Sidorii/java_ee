package ua.goit.javaee;

import ua.goit.javaee.dao.jdbc.JdbcCustomerDaoImpl;
import ua.goit.javaee.model.Customer;

/**
 * Created by Иван on 12.01.2017.
 */
public class MainForCustomers {

    public static void main(String[] args) {

        JdbcCustomerDaoImpl jdbcCustomerDao = JdbcCustomerDaoImpl.getInstance();

        Customer customer = new Customer(10, "Ivan1997");

        jdbcCustomerDao.save(customer);

        Customer customer1 = jdbcCustomerDao.findById(1);
        System.out.println("Customer with id = 1 is " + customer1);

        System.out.println("Customers in project with id = 5:");
        jdbcCustomerDao.findByProjectId(5).forEach(System.out::println);

        System.out.println("All customers: ");
        jdbcCustomerDao.getAll().forEach(System.out::println);

        Customer customer2 = jdbcCustomerDao.findById(5);
        jdbcCustomerDao.delete(customer2);

    }
}
