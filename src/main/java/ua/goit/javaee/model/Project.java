package ua.goit.javaee.model;

import java.util.Set;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public class Project extends NamedEntity {

  private int cost;
  private Set<Developer> developers;
  private Set<Customer> customers;

  public Project(Integer id, String name, int cost, Set<Developer> developers, Set<Customer> customers) {
    super(id, name);
    this.cost = cost;
    this.developers = developers;
    this.customers = customers;
  }

  public int getCost() {
    return cost;
  }

  public void setCost(int cost) {
    this.cost = cost;
  }

  public Set<Developer> getDevelopers() {
    return developers;
  }

  public void setDevelopers(Set<Developer> developers) {
    this.developers = developers;
  }

  public Set<Customer> getCustomers() {
    return customers;
  }

  public void setCustomers(Set<Customer> customers) {
    this.customers = customers;
  }

  @Override
  public String toString() {
    return "Project{" +
        "id=" + getId() +
        ", name='" + getName() + '\'' +
        ", cost=" + cost +
        ", developers=" + developers +
        ", customers=" + customers +
        '}';
  }
}
