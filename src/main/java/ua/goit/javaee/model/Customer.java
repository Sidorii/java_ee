package ua.goit.javaee.model;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public class Customer extends NamedEntity {

  public Customer(int id, String name) {
    super(id, name);
  }

  @Override
  public String toString() {
    return "Customer{" +
        "id=" + getId() +
        ", name='" + getName() + '\'' +
        '}';
  }

}
