package ua.goit.javaee.model;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
class BaseEntity {

  private Integer id;

  BaseEntity(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public boolean isNew() {
    return this.id == null;
  }

}
