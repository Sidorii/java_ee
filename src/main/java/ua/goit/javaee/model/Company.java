package ua.goit.javaee.model;

import java.util.Set;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
public class Company extends NamedEntity {

  private Set<Project> projects;

  public Company(int id, String name, Set<Project> projects) {
    super(id, name);
    this.projects = projects;
  }

  public Set<Project> getProjects() {
    return projects;
  }

  public void setProjects(Set<Project> projects) {
    this.projects = projects;
  }

  @Override
  public String toString() {
    return "Company{" +
        "id=" + getId() +
        ", name='" + getName() + '\'' +
        ", projects=" + projects +
        '}';
  }

}
